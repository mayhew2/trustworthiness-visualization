Greetings!

To make a new visualization, follow the following steps.

Run makeAnswerHist.py as follows

   $ ./makeAnswerHist.py <response file> <judgment file>

The <response file> looks like unionSingleAnswers.

To get the answer file, do the following:

   * Run mySFScorer with 'trace' flag
   * This gives a large amount of output
   * Copy this into a file, and this is the answer file
   * Ony copy lines that look like: <Letter> <query:slot> <doc:answer>
   
This is cool.
