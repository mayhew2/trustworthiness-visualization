

// everything that needs to be loaded at the beginning goes here.
$( document ).ready(function() {
    // This makes the info box draggable.
    $(".info").draggable();

    // to make the div hide
    stopSpinner();

    $("#selectDataBox").change(updateSVG);

    // This populates the data box from the filelist.json file
    d3.json("filelist.json", function (error, folderList) {
        var listItems = "<option value='dummy'>Select data source...</option>";
        for (var i = 0; i < folderList.Table.length; i++) {
            listItems += "<option value='" + folderList.Table[i].name + "'>" + folderList.Table[i].name.slice(0, -4) + "</option>";
        }
        d3.select("#selectDataBox").html(listItems);
    });

// for changing the color scheme
    $('p.color').click(function() {
        $(".color").removeClass("selected");
        $(this).addClass("selected");

        var cc = getSelectedClass();

        $(".correct").attr("class", "correct " + cc[0]);
        $(".wrong").attr("class", "wrong " + cc[1]);

    });

    $("#simple").on("click", function() {

        $("#helpModal").modal("toggle");
        console.log("clicked on the button...");
    });

});

function updateSVG() {

    startSpinner();
    var e = document.getElementById("selectDataBox");
    var strVal = e.options[e.selectedIndex].value;
    console.log(strVal);

    var filename;
    if (strVal === "dummy"){
        return;
    }
    filename = strVal;

    var w = 7300,
        h = 1000,
        p = [20, 50, 50, 20],
        x = d3.scale.ordinal().rangeBands([0, w - p[1] - p[3]], 0, 10),
        y = d3.scale.linear().range([0, h - p[0] - p[2]]),
        yscale = d3.scale.linear().range([0, h - p[0] - p[2]]),
        y0 = d3.scale.ordinal().rangeRoundBands([h-p[0]-p[2], 0], .2),
        y1 = d3.scale.linear();
    vertlines = d3.scale.ordinal().rangeBands([0, w- p[1] - p[3]], 0, 10);

    d3.select("svg").remove();
    d3.select(".copy").remove();

    var svg = d3.select("body").insert("svg:svg", "div.legend")
        .attr("width", w)
        .attr("height", h)
        .append("svg:g")
        .attr("transform", "translate(" + p[3] + "," + (h - p[2]) + ")");

    var copystring = "<div class='copy'>&copy; Stephen Mayhew, 2015</div>";
    $(copystring).insertAfter("svg");

    // Actually read the data
    d3.csv(filename, function(data) {
        var syslist = Object.keys(data[0]);
        syslist.splice(0,1);

        // this has to do with colors
        var z = d3.scale.ordinal()
            .domain(syslist)
            .range(colorbrewer.Accent[8]
                .concat(colorbrewer.Paired[12])
                .concat(colorbrewer.Spectral[7]));

        // Transpose the data into layers by system.
        var systems = d3.layout.stack()(
            syslist.map(function(system) {
                return data.map(function(d) {
                    return {x: d.slot, y: yesorno(d[system]), ans:d[system]};
                });
            }));

        // Compute the x-domain (by date) and y-domain (by top).
        x.domain(systems[0].map(function(d) { return d.x; }));
        vertlines.domain(range(0,data.length));
        y.domain([0, d3.max(systems[systems.length - 1], function(d) { return d.y0 + yesorno(d.y); })]);
        yscale.domain([0, systems.length + 4]);
        y0.domain(syslist);
        y1.domain([0, 1]).range([y0.rangeBand(), 0]);


        var rectheight = 15;

        console.log((systems.length+6)*rectheight);

        // Add y-axis rules ------------------------------
        var rule = svg.selectAll("g.rule")
            .data(yscale.ticks(10))
            .enter().append("svg:g")
            .attr("class", "rule")
            .attr("transform", function(d) { return "translate(0," + -rectheight*(d) + ")"; });

        rule.append("svg:line")
            .attr("x2", w) // - p[1] - p[3])
            .style("stroke", "#CCC")
            .style("stroke-opacity", function(d) { return d ? .7 : null; });

        rule.append("svg:text")
            .attr("x", 0) // - p[1] - p[3] + 6)
            .attr("dy", ".35em")
            .text(d3.format(",d"));
        // ---------------------------------------------

        // Add a group for each system.
        var system = svg.selectAll("g.system")
            .data(systems)
            .enter().append("svg:g")
            .attr("name", function(d, i) { return syslist[i]; } )
            .attr("class", "system")
            .style("fill", function(d, i) { return z(i); })
            .style("stroke", function(d, i) { return d3.rgb(z(i)).darker(); })
            .on("mouseover", function(d,i) {d3.select("#info #system").text(syslist[i])})
            .on("mouseout", clear)
            .on("click", function(d,i) {console.log(syslist[i])});


        // having this out here saves a LOT of time.
        // this call is slow.
        var cc = getSelectedClass();

        function getClass(ansstring){

            var char1 = ansstring.slice(0,1);

            if (char1 === "C"){
                return "correct " + cc[0];
            }else if(char1 === "X"){
                return "wrong " + cc[1];
            }else if(char1 === "R"){
                return "wrong " + cc[1];
            }else{
                return "wrong " + cc[1];
            }
        }

        // Add a rect for each claim
        var rect = system.selectAll("rect")
            .data(Object) // this is just the identity function?
            .enter().append("svg:rect")
            .attr("x", function(d) { return x(d.x); })
            .attr("y", function(d) { return -rectheight*(d.y0) - rectheight*(d.y); })
            .attr("class", function(d) { return getClass(d.ans); })
            .attr("height", function(d) { return rectheight*(d.y); })
            .attr("width", x.rangeBand())
            .on("mouseover", setClaim)
            .on("click", function(d){console.log(d.x + " " +  d.ans)});


        function transitionMultiples() {
            var t = svg; //.transition().duration(750),
            var g = t.selectAll(".system").attr("transform", function(d, i) { return "translate(0, " + (-y0(syslist[syslist.length - 1 - i])) + ")"; });
            g.selectAll("rect").attr("y", function(d) { return y1(d.y); });
            d3.selectAll(".rule").style("visibility", "hidden");
        }

        function transitionStacked() {
            var t = svg; //.transition().duration(750),
            var g = t.selectAll(".system").attr("transform", "translate(0,0)");
            g.selectAll("rect").attr("y", function (d) {
                return -rectheight * (d.y0) - rectheight * (d.y);
            });
            d3.selectAll(".rule").style("visibility", "visible");
        }

        // this is called when the sort button is pressed.
        function update(){
            startSpinner();

            d3.selectAll(".vertrule").remove();

            var e = document.getElementById("selectBox");
            var strVal = e.options[e.selectedIndex].value;

            var lines = false;

            var linelocations;
            if (strVal == "query"){
                data.sort(compByQuery);
                lines = true;
                linelocations = getLineLocs(data, queryEqual);
            }else if (strVal == "slot"){
                data.sort(compBySlot);
                lines = true;
                linelocations = getLineLocs(data, slotEqual);
            }else if (strVal == "numAns"){
                data.sort(comp2);
            }else if (strVal == "numCorrect"){
                data.sort(compByCorrect);
            }else{
                alert("No good way to sort??");
            }

            if (lines) {

                var rule2 = svg.selectAll("g.vertrule")
                    .data(linelocations)
                    .enter().append("svg:g")
                    .attr("class", "vertrule")
                    .attr("transform", function(d) { return "translate(" + (vertlines(d)) + ",0)"; });
                rule2.append("svg:line")
                    .attr("y1", 0)
                    .attr("y2", -h) // - p[1] - p[3])

                    .style("stroke", "#CCC")
                    .style("stroke-opacity", .7);
            }

            var systems = d3.layout.stack()(syslist.map(function(system) {
                return data.map(function(d) {
                    return {x: d.slot, y: +d[system]};
                });
            }));

            // Compute the x-domain (by date) and y-domain (by top).
            x.domain(systems[0].map(function(d) { return d.x; }));

            var system = svg.selectAll("g.system");
            var rect = system.selectAll("rect")
                .data(Object)
                //.transition().duration(750) // commented out because it takes too much memory.
                .attr("x", function(d) { return x(d.x) } );

            stopSpinner();

        }

        $("#sortbutton").off().on("click", update);


        $("#togglebutton").html("<i class=\"fa fa-th\"></i> To Matrix");

        $("#togglebutton").off().on("click",
            function () {
                startSpinner();
                var t = $(this).text();
                console.log(t);
                if (t.indexOf("Matrix") > -1) {
                    transitionMultiples();
                    $(this).html("<i class=\"fa fa-bar-chart\"></i> To Histogram");
                } else {
                    transitionStacked();
                    $(this).html("<i class=\"fa fa-th\"></i> To Matrix");
                }
                stopSpinner();
                $(this).blur();
            });

        stopSpinner();
        $("#selectBox").prop("disabled", false);
        $("#sortbutton").prop("disabled", false);
        $("#togglebutton").prop("disabled", false);
    });



}

// get the indices for the line numbers
function getLineLocs(data, equalFunc){
    var arr = [0];
    for(var i = 0; i < data.length-1; i++){
        if (!equalFunc(getSlot(data[i]), getSlot(data[i+1]))){
            arr.push(i+1);
        }
    }
    arr.push(data.length);
    return arr;
}








