var data = [4, 8, 33, 15, 16, 45, 23, 42];

var chart = d3.select("body")
    .append("svg:svg")
    .attr("class", "chart")
    .attr("width", 420)
    .attr("height", 20*data.length)

var x = d3.scale.linear()
     .domain([0, d3.max(data)])
     .range([0, 420]);

var y = d3.scale.ordinal()
     .domain(data)
     .rangeBands([0, 20*data.length]);

// var rect = chart.selectAll("rect")
//     .data(data)
//     .enter().append("svg:rect")
//     .attr("class", "myrect")
//     .attr("y", function(d, i) { return i*20 })
//     .attr("width", x)
//     .attr("height", 20);

chart.selectAll("rect")
    .data(data)
   .enter().append("svg:rect")
     .attr("y", y)
     .attr("width", x)
     .attr("height", y.rangeBand());

chart.selectAll("text")
     .data(data)
   .enter().append("svg:text")
     .attr("x", x)
    .attr("y", function(d) { return y(d) + y.rangeBand() / 2; })
     .attr("dx", -3) // padding-right
     .attr("dy", "0.35em") // vertical-align: middle
     .attr("text-anchor", "end") // text-align: right
     .text(String);

function redraw() {

    console.log(data);
    data.sort(function(a,b){return a-b}); // this is here so that they are converted to ints
    console.log(data);
    
    // Update
    chart.selectAll("rect")
        .data(data)
        .transition()
        .duration(1000)
        .attr("width", x);
    // .attr("y", function(d) { return h - y(d.value) - .5; })
    // .attr("height", function(d) { return y(d.value); });

    chart.selectAll("text")
        .data(data)
        .transition()
        .duration(1000)
        // .attr("y", function(d) { return y(d) + y.rangeBand() / 2; })
        .attr("x", x)
        .text(String);    
}

console.log(rect);