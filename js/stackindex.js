// OLD NOT USED!!

// <!-- var parseDate = d3.time.format("%Y-%m").parse, -->
    // <!-- formatYear = d3.format("02d"), -->
    // <!-- formatDate = function(d) { return "Q" + ((d.getMonth() / 3 | 0) + 1) + formatYear(d.getFullYear() % 100); }; -->

// Define a margin, width, and height
var margin = {top: 10, right: 20, bottom: 20, left: 60},
    width = 1700 - margin.left - margin.right,
    height = 1000 - margin.top - margin.bottom;

var y0 = d3.scale.ordinal().rangeRoundBands([height, 0], .2);

var y1 = d3.scale.linear();

var x = d3.scale.ordinal()
    .rangeRoundBands([0, width], .1, 0);

var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom");

var nest = d3.nest()  // what does nest do?
    .key(function(d) { return d.system; });

// What do all these do?
var stack = d3.layout.stack()
    .values(function(d) { return d.values; })
    .x(function(d) { return d.slot; })
    .y(function(d) { return d.value; })
    .out(function(d, y0) { d.valueOffset = y0; });

// Define colors
var color = d3.scale.category20();

var svg = d3.select("body").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

// Read in the data
d3.csv("mycsv2.csv", function(error, data) {

    // What is this actually doing?
    // Just converting d.value into numbers?
    data.forEach(function(d) {
        d.slot = d.slot; //parseDate(d.date);
        d.value = +d.value;
    });

    // uses nest
    var dataBySystem = nest.entries(data);
    console.log(dataBySystem.map(function(d) { return d.key; }));
    
    // uses stack
    stack(dataBySystem);
    x.domain(dataBySystem[0].values.map(function(d) { return d.slot; }));
    y0.domain(dataBySystem.map(function(d) { return d.key; }));
    y1.domain([0, d3.max(data, function(d) { return d.value; })]).range([y0.rangeBand(), 0]);

    console.log(d3.max(data, function(d) { return d.value; }));
    
    // select everything with class system
    var system = svg.selectAll(".system")
        .data(dataBySystem) // what is this?
        .enter().append("g") // what is this?
        .attr("class", "system")
        .attr("transform", function(d) { return "translate(0," + y0(d.key) + ")"; });

    // add text to each system
    system.append("text")
        .attr("class", "system-label")
        .attr("x", 6)
        .attr("y", function(d) { return y1(d.values[0].value / 2); })
        .attr("dy", ".35em")
        .text(function(d) { return d.key; });
    
    // this, I think is the main important thing.
    system.selectAll("rect") // this is an array of 27 values
        .data(function(d) { return d.values; })
        .enter().append("rect")
        .style("fill", function(d) { return color(d.system); })
        .attr("x", function(d) { return x(d.slot); })
        .attr("y", function(d) { return y1(d.value); })
        .attr("width", x.rangeBand())
        .attr("height", function(d) { return y0.rangeBand() - y1(d.value); });
    
    system.filter(function(d, i) { return !i; }).append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + y0.rangeBand() + ")")
        .call(xAxis);
    
    d3.selectAll("input").on("change", change);
    
    var timeout = setTimeout(function() {
        d3.select("input[value=\"stacked\"]").property("checked", true).each(change);
    }, 2000);
    
    function change() {
        clearTimeout(timeout);
        if (this.value === "multiples") transitionMultiples();
        else transitionStacked();
    }
    
    function transitionMultiples() {
        var t = svg.transition().duration(750),
        g = t.selectAll(".system").attr("transform", function(d) { return "translate(0," + y0(d.key) + ")"; });
        g.selectAll("rect").attr("y", function(d) { return y1(d.value); });
        g.select(".system-label").attr("y", function(d) { return y1(d.values[0].value / 2); })
    }
    
    function transitionStacked() {
        var t = svg.transition().duration(750),
        g = t.selectAll(".system").attr("transform", "translate(0," + y0(y0.domain()[0]) + ")");
        g.selectAll("rect").attr("y", function(d) { return y1(d.value + d.valueOffset); });
        g.select(".system-label").attr("y", function(d) { return y1(d.values[0].value / 2 + d.values[0].valueOffset); })
    }
});
