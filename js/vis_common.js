/**
 * Created by stephen on 3/20/15.
 */
function startSpinner() {
    console.log("Starting spinner...")
    $("#spinnerdiv").show();
    $("#spinner").addClass("fa-spin");


}

function stopSpinner() {
    console.log("Stopping spinner...")
    $("#spinnerdiv").hide();
    $("#spinner").removeClass("fa-spin");
}

function getSelectedClass(){
    // should only be one selected at a time...
    var kids = $(".selected").children();

    if(kids.length == 0){
        var correct_class= "";
        var wrong_class = "";
    }else{
        var correct_class = kids[0].className.split(" ")[1];
        var wrong_class = kids[1].className.split(" ")[1];
    }

    return [correct_class, wrong_class];
};

// used later
function yesorno(dy){
    if (dy === "0" || +dy == 0){
        return 0;
    }else{
        return 1;
    }
}

function numOnes(dict){
    var sum= 0;
    for (var key in dict){
        if (key == "slot") {
            continue
        }
        // sum += parseInt(dict[key]);
        sum += yesorno(dict[key]);
    }
    return sum;
}

// This sorts by the number of answers
function comp(a,b){
    var num_a = numOnes(a),
        num_b = numOnes(b);

    if (num_a < num_b){
        return 1;
    }
    if (num_a > num_b){
        return -1;
    }
    return 0;
}

// This sorts by the number of answers, then by number correct
function comp2(a,b){
    var num_a = numOnes(a),
        num_b = numOnes(b);

    if (num_a < num_b){
        return 1;
    }
    if (num_a > num_b){
        return -1;
    }
    return compByCorrect(a,b);
}



function numCorrect(dict){
    var sum = 0;
    for (var key in dict){
        if (key == "slot"){continue}
        var firstChar = dict[key].charAt(0);
        if (firstChar == "C"){
            sum += 1;
        }
    }
    return sum;
}

function compByCorrect(a,b){
    var num_a = numCorrect(a),
        num_b = numCorrect(b);

    if (num_a < num_b){
        return 1;
    }
    if (num_a > num_b){
        return -1;
    }
    return comp(a, b);
}


function getSlot(dict){
    for (var key in dict){
        if (key == "slot"){
            return dict[key];
        }
    }
    return null;
}

// This compares by SF_ENG_062...
function compByQuery(a,b){
    var query_a = getSlot(a).split(":").slice(0,1),
        query_b = getSlot(b).split(":").slice(0,1);

    if (query_a < query_b){
        return 1;
    }
    if (query_a > query_b){
        return -1;
    }
    return comp(a,b);
}

// This compares by org:founder...
function compBySlot(a,b){
    var slot_a = getSlot(a).split(":").slice(1).join(":") ,
        slot_b = getSlot(b).split(":").slice(1).join(":");

    if (slot_a < slot_b){
        return 1;
    }
    if (slot_a > slot_b){
        return -1;
    }
    return comp(a,b);
}

function range(start, stop, step){
    if (typeof stop=='undefined'){
        // one param defined
        stop = start;
        start = 0;
    }
    if (typeof step=='undefined'){
        step = 1;
    }
    if ((step>0 && start>=stop) || (step<0 && start<=stop)){
        return [];
    }
    var result = [];
    for (var i=start; step>0 ? i<stop : i>stop; i+=step){
        result.push(i);
    }
    return result;
}

function setClaim(d){
    d3.select("#info #claim").text(d.x);
    d3.select("#info #answer").text(d.ans);
}

function clear(){
    d3.select("#info #system").text("");
    d3.select("#info #claim").text("");
    d3.select("#info #answer").text("");
}

// return true if the two strings have the same slot
function slotEqual(s1,s2){
    var slot1 = s1.split(":").slice(1).join(":") ,
        slot2 = s2.split(":").slice(1).join(":");
    return slot1 === slot2;
}

// return true if the two strings have the same query
function queryEqual(s1, s2){
    var slot1 = s1.split(":")[0],
        slot2 = s2.split(":")[0];
    return slot1 === slot2;
}



